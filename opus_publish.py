# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OpusPublisher
                                 A QGIS plugin
 This plugin publishes QGIS Layers to OpusMaps
                              -------------------
        begin                : 2018-03-19
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Bluefox / OpusMaps
        email                : rgshepherd@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon,QMessageBox
from qgis._core import QgsMessageLog
from qgis.core import QgsMessageLog
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from opus_publish_dialog import OpusPublisherDialog
from opus_publish_finished import OpusPublisherFinished
from opus_exporter import OpusExporter
from opus_publisher import OpusNetworkPublisher
import os.path

from utils import open_file_explorer


class OpusPublisher:
    """QGIS Plugin Implementation."""

    NETWORK_PUBLISH = False

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        #locale = QSettings().value('locale/userLocale')[0:2]
        #locale_path = os.path.join(
        #    self.plugin_dir,
        #    'i18n',
        #    'OpusPublisher_{}.qm'.format(locale))

        #if os.path.exists(locale_path):
        #    self.translator = QTranslator()
        #    self.translator.load(locale_path)

        #    if qVersion() > '4.3.3':
        #        QCoreApplication.installTranslator(self.translator)


        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Opus Publisher')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'OpusPublisher')
        self.toolbar.setObjectName(u'OpusPublisher')

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('OpusPublisher', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = OpusPublisherDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/OpusPublisher/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Publish to Opus'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Opus Publisher'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            exporter = OpusExporter(self.iface)
            exp_file = exporter.export()

            if exp_file is not None:
                QgsMessageLog.logMessage("Exported to file: {}".format(exp_file), "OpusPublisher")
                if self.NETWORK_PUBLISH:
                    publisher = OpusNetworkPublisher(self.iface, exp_file, on_complete )
                    publisher.publish()
                else:
                    open_file_explorer(exp_file)


def on_complete(file2):
    QgsMessageLog.logMessage("Filename to delete: {}".format(file2), "OpusPublisher")

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Complete")
    msg.setInformativeText("Published Successfully.")
    msg.setDetailedText("Archive: " + file2 + "\n\nClick [Cancel] to keep the file above. Or [Close] to delete")
    msg.setWindowTitle("Notice")
    msg.setStandardButtons(QMessageBox.Cancel | QMessageBox.Close)

    result = msg.exec_()
    if result:
        os.unlink(file2)

