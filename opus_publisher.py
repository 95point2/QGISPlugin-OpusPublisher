from qgis.gui import QgisInterface

from PyQt4.QtCore import QUrl, QVariant, QFile, QIODevice, Qt
from PyQt4.QtNetwork import QNetworkRequest, QHttpMultiPart, QHttpPart
from PyQt4.QtGui import QDialog, QLabel
from qgis.core import QgsNetworkAccessManager


class OpusNetworkPublisher:

    def __init__(self, qgis, exp_file, callback):
        """Constructor.

        :param qgis: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type qgis: QgisInterface
        """
        self.qgis = qgis
        self.exp_file = exp_file
        self.callback = callback
        self.wait_dialog = None

    def publish(self):



        networkAccessManager = QgsNetworkAccessManager.instance()
        networkAccessManager.finished.connect(self.url_call_finished)

        multiPart = QHttpMultiPart(QHttpMultiPart.FormDataType)

        text_part = QHttpPart()
        text_part.setHeader(QNetworkRequest.ContentDispositionHeader, "form-data; name=\"text\"")
        text_part.setBody("Optional Text Data")

        zip_part = QHttpPart()
        zip_part.setHeader(QNetworkRequest.ContentTypeHeader, "application/zip")
        zip_part.setHeader(QNetworkRequest.ContentDispositionHeader, "form-data; name=\"zip\"; filename=\"upload.zip\"")

        upload_file = QFile(self.exp_file)
        upload_file.open(QIODevice.ReadOnly)

        zip_part.setBodyDevice(upload_file)
        upload_file.setParent(multiPart)

        multiPart.append(text_part)
        multiPart.append(zip_part)

        url = QUrl("http://ptsv2.com/t/bfoppub/post")
        request = QNetworkRequest(url)

        reply = networkAccessManager.post(request, multiPart)
        multiPart.setParent(reply)

        self.wait_dialog = QDialog()
        self.wait_dialog.setMinimumWidth(400)
        b1 = QLabel(self.wait_dialog)
        b1.setText("Uploading, Please Wait...")
        b1.move(50, 50)
        self.wait_dialog.setWindowTitle("Please Wait")
        self.wait_dialog.setWindowModality(Qt.ApplicationModal)
        self.wait_dialog.exec_()

    def url_call_finished(self, reply):
        resp = reply.readAll()
        self.wait_dialog.done(1)
        self.callback(self.exp_file)
        reply.deleteLater()



