from .utils import filter_attributes


def test_filter_attributes():
    """ existing_field_list = ['a', 'b', 'c', 'd']        ## ordered field list from existing layer
        existing_value_list = ['myA', 'myB', 'myC']       ## values in order above
        new_field_list = ['w', 'x', 'y', 'z']             ## ordered field list for opus layer
        mapping = { 'w':None, 'x':'a', 'y':'c', 'z':'b' } ## mapping from opus to
        return [ None, 'myA', 'myC', 'myB' ]              ## values in  new_field_list order
    """
    res = filter_attributes(['a', 'b', 'c', 'd'],
                            ['myA', 'myB', 'myC'],
                            ['w', 'x', 'y', 'z'],
                            {'w': None, 'x': 'a', 'y': 'c', 'z': 'b'})

    assert res == [ None, 'myA', 'myC', 'myB' ]
    assert res != [ None, 'myB', 'myC', 'myB', 'Rob']

