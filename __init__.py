# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OpusPublisher
                                 A QGIS plugin
 This plugin publishes QGIS Layers to OpusMaps
                             -------------------
        begin                : 2018-03-19
        copyright            : (C) 2018 by Bluefox / OpusMaps
        email                : rgshepherd@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load OpusPublisher class from file OpusPublisher.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .opus_publish import OpusPublisher
    return OpusPublisher(iface)
