# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Opus Publisher
qgisMinimumVersion=2.0
description=This plugin publishes QGIS Layers to OpusMaps
version=1.1
author=Bluefox Technologies
email=rgshepherd@gmail.com

about=Exports QGIS Layers to OpusMaps

#tracker=
#repository=
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=python

homepage=https://www.opusmap.co.uk
category=Plugins
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

