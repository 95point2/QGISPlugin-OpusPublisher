import os
import platform
import subprocess

def filter_attributes(existing_field_list, existing_value_list, new_field_list, mapping):
    ''' existing_field_list = ['a', 'b', 'c', 'd']        ## ordered field list from existing layer
        existing_value_list = ['myA', 'myB', 'myC']       ## values in order above
        new_field_list = ['w', 'x', 'y', 'z']             ## ordered field list for opus layer
        mapping = { 'w':None, 'x':'a', 'y':'c', 'z':'b' } ## mapping from opus to
        return [ None, 'myA', 'myC', 'myB' ]              ## values in  new_field_list order
    '''

    new_value_list = []

    for new_field in new_field_list:
        existing_field_lookup = mapping[new_field]
        if existing_field_lookup == None:
            new_value_list.append(None)
        else:
            existing_field_index = existing_field_list.index(existing_field_lookup)
            new_value_list.append(existing_value_list[existing_field_index])

    return new_value_list


def open_file_explorer(path):
    if platform.system() == "Windows":
        os.startfile(path)
    elif platform.system() == "Darwin":
        subprocess.Popen(["open", "-R", path])
    else:
        subprocess.Popen(["xdg-open", path])