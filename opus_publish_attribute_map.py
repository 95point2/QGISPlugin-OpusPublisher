# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OpusPublisherDialog
                                 A QGIS plugin
 This plugin publishes QGIS Layers to OpusMaps
                             -------------------
        begin                : 2018-03-19
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Bluefox / OpusMaps
        email                : rgshepherd@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'attribute_map.ui'))


class OpusAttMapDialog(QtGui.QDialog, FORM_CLASS):
    NONE_VALUE = "<none>"

    def __init__(self, title, parent=None):
        """Constructor."""
        super(OpusAttMapDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        self.setWindowTitle("Map Attributes for layer: " + title)


    def set_layer_attributes(self, items):
        self.attSel_title.addItems(items)

        items = list(items)
        items.insert(0, self.NONE_VALUE)
        self.attSel_desc.addItems(items)
        self.attSel_pol.addItems(items)
        self.attSel_url.addItems(items)

    def get_layer_mappings(self):
        mapping = dict()
        mapping['title'] = self.attSel_title.currentText() if self.attSel_title.currentText() != self.NONE_VALUE else None
        mapping['desc'] = self.attSel_desc.currentText() if self.attSel_desc.currentText() != self.NONE_VALUE else None
        mapping['policy'] = self.attSel_pol.currentText() if self.attSel_pol.currentText() != self.NONE_VALUE else None
        mapping['url'] = self.attSel_url.currentText() if self.attSel_url.currentText() != self.NONE_VALUE else None
        return mapping
