# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'opus_publish_finished_ui.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_OpusPublisherFinished(object):
    def setupUi(self, OpusPublisherFinished):
        OpusPublisherFinished.setObjectName(_fromUtf8("OpusPublisherFinished"))
        OpusPublisherFinished.resize(799, 98)
        self.filenameLabel = QtGui.QLabel(OpusPublisherFinished)
        self.filenameLabel.setGeometry(QtCore.QRect(10, 20, 781, 31))
        self.filenameLabel.setFrameShape(QtGui.QFrame.StyledPanel)
        self.filenameLabel.setText(_fromUtf8(""))
        self.filenameLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.filenameLabel.setObjectName(_fromUtf8("filenameLabel"))
        self.button_box = QtGui.QDialogButtonBox(OpusPublisherFinished)
        self.button_box.setGeometry(QtCore.QRect(600, 60, 181, 32))
        self.button_box.setOrientation(QtCore.Qt.Horizontal)
        self.button_box.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.button_box.setObjectName(_fromUtf8("button_box"))
        self.label = QtGui.QLabel(OpusPublisherFinished)
        self.label.setGeometry(QtCore.QRect(20, 70, 351, 16))
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(OpusPublisherFinished)
        QtCore.QObject.connect(self.button_box, QtCore.SIGNAL(_fromUtf8("accepted()")), OpusPublisherFinished.accept)
        QtCore.QObject.connect(self.button_box, QtCore.SIGNAL(_fromUtf8("rejected()")), OpusPublisherFinished.reject)
        QtCore.QMetaObject.connectSlotsByName(OpusPublisherFinished)

    def retranslateUi(self, OpusPublisherFinished):
        OpusPublisherFinished.setWindowTitle(_translate("OpusPublisherFinished", "Opus Publisher", None))
        self.label.setText(_translate("OpusPublisherFinished", "Click [Cancel] to retain file for inspection, [OK] to discard", None))

