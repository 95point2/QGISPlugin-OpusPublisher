# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OpusPublisherDialog
                                 A QGIS plugin
 This plugin publishes QGIS Layers to OpusMaps
                             -------------------
        begin                : 2018-03-19
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Bluefox / OpusMaps
        email                : rgshepherd@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from qgis.core import QgsMessageLog

from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'opus_publish_finished_ui.ui'))


class OpusPublisherFinished(QtGui.QDialog, FORM_CLASS):
    def __init__(self, filename, parent=None):
        """Constructor."""
        super(OpusPublisherFinished, self).__init__(parent)

        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        QgsMessageLog.logMessage("HERE1: {}".format(filename), "OpusPublisher")

        self.filenameLabel.setText(filename)
