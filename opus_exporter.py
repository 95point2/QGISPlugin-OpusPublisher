import os
import zipfile
import json
from tempfile import mkdtemp,mkstemp
from shutil import rmtree, make_archive
from qgis.core import QgsVectorLayer, QgsMapLayer, QgsVectorFileWriter, QgsMessageLog, QgsFeature, QgsField, QgsWKBTypes
from qgis.gui import QgisInterface
from PyQt4.QtGui import QMessageBox
from PyQt4.QtCore import QVariant

from utils import filter_attributes
from opus_publish_attribute_map import OpusAttMapDialog

class OpusExporter:

    def __init__(self, qgis):
        """Constructor.

        :param qgis: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type qgis: QgisInterface
        """
        self.qgis = qgis
        self.zipfile = None

    def export(self):
        export_dir = mkdtemp(".zip.d", "opus_")

        opus_field_list = ['title', 'desc', 'policy', 'url']

        export_meta = {'layers': []}

        found_one = False # anything exported?

        layers = self.qgis.legendInterface().layers()
        group_rels = self.qgis.legendInterface().groupLayerRelationship()
        group_lookup = make_layer_group_index(group_rels)

        export_meta['groups'] = group_lookup

        for layer in layers:
            if layer.type() == QgsMapLayer.VectorLayer:

                layer_meta = {
                    'id': layer.id(),
                    'name': layer.name(),
                    'group': group_lookup[layer.id()],
                    'visible': self.qgis.legendInterface().isLayerVisible(layer),
                    'shapefile': "layer-" + layer.id() + ".shp",
                    'style': "layer-" + layer.id() + ".qml"
                }

                export_meta['layers'].append(layer_meta)

                layer_geom_type = QgsWKBTypes.displayString(int(layer.wkbType()))
                layer_geom_crs = layer.crs().authid()

                QgsMessageLog.logMessage("Layer {} - {}".format(layer_geom_type, layer_geom_crs), "OpusPublisher")

                found_one = True

                fields = [field.name() for field in layer.dataProvider().fields()]

                dlg_map_attr = OpusAttMapDialog(layer.name())
                dlg_map_attr.set_layer_attributes(fields)
                dlg_map_attr.show()
                res = dlg_map_attr.exec_()

                QgsMessageLog.logMessage("Mapping Result: {}".format(res), "OpusPublisher")

                layer_attr_map = dlg_map_attr.get_layer_mappings()

                QgsMessageLog.logMessage("Mapping: {}".format(layer_attr_map), "OpusPublisher")

                newFeats = []

                for feat in layer.getFeatures():
                    QgsMessageLog.logMessage("Feature: {}".format(feat), "OpusPublisher")
                    newFeat = QgsFeature(feat)
                    existing_attrs = newFeat.attributes()

                    QgsMessageLog.logMessage("Feature Exs Attr: {}".format(existing_attrs), "OpusPublisher")

                    new_attrs = filter_attributes(fields, existing_attrs, opus_field_list, layer_attr_map )

                    QgsMessageLog.logMessage("Feature: New Attr {}".format(new_attrs), "OpusPublisher")

                    newFeat.setAttributes(new_attrs)
                    QgsMessageLog.logMessage("New Feature: {}".format(newFeat.attributes()), "OpusPublisher")
                    newFeats.append(newFeat)


                mem_layer = QgsVectorLayer(layer_geom_type + "?crs=" + layer_geom_crs, "dupl-"+layer.id(), "memory")
                mem_layer_data = mem_layer.dataProvider()

                new_fields = []
                for of in opus_field_list:
                    f = QgsField()
                    f.setName(of)
                    f.setType(QVariant.String)
                    f.setLength(5000)
                    f.setPrecision(0)
                    f.setTypeName("String")
                    new_fields.append(f)

                mem_layer_data.addAttributes(new_fields)
                mem_layer.updateFields()
                mem_layer_data.addFeatures(newFeats)

                output = os.path.join(export_dir, layer_meta['shapefile'])
                QgsVectorFileWriter.writeAsVectorFormat(mem_layer, output, "utf-8", None, "ESRI Shapefile")

                layer.saveNamedStyle(os.path.join(export_dir, layer_meta['style']))

        if found_one:

            export_meta_file = open(os.path.join(export_dir, 'metadata.json'), "w")
            export_meta_str = json.dumps(export_meta)
            QgsMessageLog.logMessage("Meta Data {}".format(export_meta_str), "OpusPublisher")
            export_meta_file.write(export_meta_str)
            export_meta_file.close()

            QgsMessageLog.logMessage("Archving {}".format(export_dir), "OpusPublisher")
            zip_filename = mkstemp(".zip", "opus_")
            QgsMessageLog.logMessage("Archiving To: {}".format(zip_filename[1]), "OpusPublisher")

            zip = zipfile.ZipFile(zip_filename[1],"w",zipfile.ZIP_DEFLATED)

            zipdir(export_dir, zip)

            self.zipfile = zip_filename[1]

            rmtree(export_dir)
            return self.zipfile
        else:
            warn("No Vector Layers Selected")
            return None

    def on_complete(self):
        if self.zipfile is not None:
            os.unlink(self.zipfile)



def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), path))

def warn(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Warning")
    msg.setInformativeText(text)
    msg.setWindowTitle("Notice")
    msg.setStandardButtons(QMessageBox.Cancel)
    return msg.exec_()

def make_layer_group_index(groupRels):
    layer_group_index = {}
    for pair in groupRels:
        group_name = pair[0]
        for layer_id in pair[1]:
            layer_group_index[layer_id] = group_name
    return layer_group_index